/*

  Mini-Activity:



  Create 5 new documents in a new fruits collection with the following fields and values: 

  (aggregation152)



  name: "Apple"

  supplier: "Red Farms Inc."

  stocks: 20

  price: 40

  onSale: true



  name: "Banana"

  supplier: "Yellow Farms"

  stocks: 15

  price: 20

  onSale: true



  name: "Kiwi"

  supplier: "Green Farming and Canning"

  stocks: 25

  price: 50

  onSale: true



  name: "Mango"

  supplier: "Yellow Farms"

  stocks: 10

  price: 60

  onSale: true



  name: "Dragon Fruit"

  supplier: "Red Farms Inc."

  stocks: 10,

  price: 60,

  onSale: true



*/

/*db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

]);*/

//Aggregation In MongoDB

  //This is the act or the process of generating manipulated data and perform 

  //operations to create filtered result that helps in data analysis.
  //This helps us in creating reports from analyzing the data provided in our documents.
  //This helps us arrive at summarized information not previously shown in our documents.

  //Aggregation Pipelines

    //Aggregation is done in 2-3 steps typically. Each stage/step is called a pipeline. 

    //The first pipeline was with the use of $match

    //$match is used to pass the documents or get the documents that will match our condition.

    //syntax: {$match: {field:value}}

    //$group is used to group elements/documents together and create an analysis of these 

    //grouped documents. 

    //In our example, we gathered and grouped our items and solved or got the sum of all their stocks.

    //syntax: {$group: {_id:<id>,fieldResult: "valueResult"}}

    //_id: this is us associating an id for the aggregated result. 

    //By adding null, we have aggregated and group all matched documents in a single aggregated result. 

    //You may also group all matched documents in a single aggregated result if you provide a specific id.

    //Hovever, when id is provided with $supplier, we created aggregated results per supplier.
    //$field will refer to a field name that is available in the documents that are being aggregated on.
    

    //$sum operator will get the sum of all the values of the given field. This will allow us

    //get the total of the values of the given field per group. It will ignore non-numerical values.
/*db.fruits.aggregate([

{$match: {onSale:true}},//query
{$group: {_id:"$supplier", totalStocks: {$sum: "$stock"}}},

//group the matched documents and arrive an aggregated result.

])*/



//Mini-Activity:



//In a new aggregation for fruits collection,

//First, match all items with supplier as Red Farms Inc.

//Second, group the matched documents in a single aggregated result of the total stocks of items

//supplied by Red Farms Inc.

//Result should be the total number of stocks for items supplied by Red Farms Inc.



/*db.fruits.aggregate([



{$match: {supplier:"Red Farms Inc."}},//query

{$group: {_id:"redFarmsIncStocks", totalStocks: {$sum: "$stocks"}}},

//group the matched documents and arrive an aggregated result.



])*/



/*db.fruits.aggregate([



    {$match: {onSale:false}},

    {$group: {_id:"notForSale", total: {$sum: "$stocks"}}}



])*/

//$avg - will allow us to get the average of the given values of a specified field

//for our grouped documents


//average stock of all items on sale per supplier

/*db.fruits.aggregate([



    {$match: {onSale:true}},

    {$group: {_id:"$supplier", avgStock: {$avg: "$stocks"}}}



])*/

    

//average price of all items on sale

/*db.fruits.aggregate([

    

    {$match: {onSale:true}},

    {$group: {_id:"avgPriceOnSale", avgPrice: {$avg: "$price"}}}

    

])*/



//$max - will allow us to get the highest value out of all the values of a given field.



//highest number of stocks for all items on sale.

/*db.fruits.aggregate([



    {$match: {onSale:true}},

    {$group: {_id:"maxStockOnSale", maxStock: {$max: "$stocks"}}}



])*/


//highest number of stocks for all items on sale per supplier
/*db.fruits.aggregate([



    {$match: {onSale:true}},

    {$group: {_id:"$supplier", maxStock: {$max: "$stocks"}}}



])*/



//$min - will allow us to get the lowest value of the values of the given field.


//lowest number of stocks for all items on sale.
/*db.fruits.aggregate([



    {$match: {onSale:true}},

    {$group: {_id:"minStockOnSale", minStock: {$min: "$stocks"}}}



])*/



//Mini-Activity:

    //Get the lowest number of stocks for all items on sale per supplier.
/*db.fruits.aggregate([

   

    {$match: {onSale:true}},

    {$group: {_id:"$supplier", minStock: {$min: "$stocks"}}}

    

])*/

    

//other stages

  //$count - is a stage we can add after a match stage to count all items 

 //that matches our criteria


//count all items on sale
/*db.fruits.aggregate([

    

    {$match: {onSale:true}},

    {$count: "itemsOnSale"}

    

])*/


//count number of items with price less than 50

/*db.fruits.aggregate([

    

    {$match: {price:{$lt:50}}},//query - you can use query operators

    {$count: "priceLessThan50"}

])*/


//Mini-Activity:

    //Aggregate to count the number of items that are not on sale.

/*    db.fruits.aggregate([

        

        {$match: {onSale:false}},

        {$count: "notForSale"}

    

    ])*/

    //Aggregate to count the number of items/fruits with stock less than 20
/*    db.fruits.aggregate([

        

        {$match: {stocks:{$lt:20}}},

        {$count: "stocksLessThan20"}

    ])*/
    

    //counts total number of items per group.

    /*db.fruits.aggregate([

    

        {$match: {onSale:true}},

        {$group: {_id:"$supplier", noOfItems: {$sum: 1}}}

        //each item in the group will be assigned a value of 1 or counted as 1 and with the $sum operator

        //we can add each item in the group and arrive at the total number of items.

    ])*/

    

    //You can have multiple fieldresults per aggregated result.

    /*db.fruits.aggregate([

        

        {$match: {onSale:true}},

        {$group: {_id:"$supplier", noOfItems: {$sum:1}, totalStock: {$sum:"$stocks"}}}

    

    ])*/

        

    //total number of items per supplier, total number of stock of items per supplier, list of items per supplier

    /*db.fruits.aggregate([

        

        {$match: {onSale:true}},

        {$group: {

            _id:"$supplier", 

            noOfItems: {$sum:1},

            totalStock:{$sum:"$stocks"},

            itemPricesPerSupplier:{$addToSet:"$price"}//accumulates and adds the given fields into an array.

          }

        }

    ])*/


//$out stage - will save/output your aggregated result from $group in a new collection.

//note: This will overwrite the collection if it already exists

    db.fruits.aggregate([

    

        {$match: {onSale:true}},

        {$group: {_id:"$supplier", totalStock: {$sum: "$stocks"}}},

        {$out: "totalStocksPerSupplier"}

    ])


