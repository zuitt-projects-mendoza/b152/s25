db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc."}},
    {$count: "itemsRedFarms"}

])

db.fruits.aggregate([
 
    {$match: {price:{$gt:50}}},
    {$count: "itemsGreaterThan50"}

])

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", avgPrice: {$avg: "$price"}}}

])

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", maxPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", minPrice: {$min: "$price"}}}

])